#!/usr/bin/env python3

# renameDates.py - Renames filenames with American MM-DD-YYYY date format to European DD-MM-YYYY.

import shutil, os, re, logging

# logging to screen
logging.basicConfig(level=logging.INFO, format=' %(asctime)s -  %(levelname)s -  %(message)s')
logging.info('Starting program')

# Create a regex that matches files with the American date format MM-DD-YYYY.
datePattern = re.compile(r"""
    ^(.*?)          # all text before the date
    ((0|1)?\d)-     # one or two digits for the month
    ((0|1|2|3)?\d)- # one or two digits for the day
    ((19|20)\d\d)   # four digits for the year
    (.*?)$          # all text after the date
    """, re.VERBOSE)

# Loop over the files in the working directory.
logging.info('Reading files to be renamed')
fileDirToRename = os.listdir('filesToRename')
for amerFileName in fileDirToRename:
    mo = datePattern.search(amerFileName)
#Skip files without a date.
    if mo == None:
        continue
#Get the different parts of the filename.
    beforePart = mo.group(1)
    monthPart  = mo.group(2)
    dayPart    = mo.group(4)
    yearPart   = mo.group(6)
    afterPart  = mo.group(8)
#Form the European-style filename.
    euroFileName = f'{beforePart}{dayPart}-{monthPart}-{yearPart}{afterPart}'
#Get the full, absolute file paths.
    filesToRename = os.path.abspath('filesToRename')
    filesRenamed = os.path.abspath('filesRenamed')
    amerFileName = os.path.join(filesToRename, amerFileName)
    euroFileName = os.path.join(filesRenamed, euroFileName)
    # print(amerFileName)
    # print(euroFileName)
#Rename the files.
    logging.info(f'Renaming "{amerFileName}" to "{euroFileName}"...')
    shutil.copy(amerFileName, euroFileName)